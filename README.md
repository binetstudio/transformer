# Čemu služi ovaj alat?
Za transformaciju pojedinačnih redova u matrici, radnja potrebna za Simplex postupak
# Dependency

NodeJS



# Instalacija
Nakon instalacije NodeJS-a (testirano na verziji LTS)
U direktoriju pokrenuti komandu

```bash
npm i
```

# Pokretanje

Nakon instalacije u direktoriju pokrenuti komandu

```bash
npm start
```

# Korištenje

Pratiti komentare u kodu. Save za pokretanje skripte.

Čitati Console log


