import { fraction, divide, add, multiply } from "mathjs"
const mather = () => {
  // upis

  const first = ['1/2',0,0,0,'1/2','-1/2',1,25]
  const second = ['-55/2',0,10,0,'-95/2','95/2',0,26125]
  const pozicija = 5

  // kraj upisa

  const indexMain = pozicija - 1
  solver(first, second, indexMain)
}

const toFraction = (e) => {
  const { s, n, d } = e
  if (s === -1 && d !== 1) return `-${n}/${d}`
  if (s === -1 && d === 1) return `-${n}`
  if (s === 1 && d === 1) return `${n}`
  if (s === 1 && d !== 1) return `${n}/${d}`
}

const solver = (first, second, indexMain) => {
  if (first.length !== second.length || !first || !second) {
    console.log(first.length)
    console.log(second.length)
    console.log("Greška pri unosu")
    return null
  }
  const firstFract = first.map((cell) => fraction(cell))
  const secondFract = second.map((cell) => fraction(cell))
  const multiplier = multiply(secondFract[indexMain], -1)
  const firstDiv = firstFract.map((cell) => divide(cell, firstFract[indexMain]))
  const solved = secondFract.map((cell, index) => {
    const factor = multiply(firstDiv[index], multiplier)
    return add(cell, factor)
  })
  const firstPrint = firstDiv.map((cell) => toFraction(cell))
  const solvedPrint = solved.map((cell) => toFraction(cell))
  const firstFractPrint = firstFract.map((cell) => toFraction(cell))
  const secondFractPrint = secondFract.map(cell => toFraction(cell))
  console.log("VODECI RED")
  console.log(firstFractPrint.join('  '))
  console.log("DRUGI RED")
  console.log(secondFractPrint.join('  '))
  console.log("VODECI RED PODIJELJEN")
  console.log(firstPrint.join('  '))
  console.log("FAKTOR ZA TRANSFORMACIJU")
  console.log(toFraction(multiplier))
  console.log("TRANSFORMIRANI DRUGI RED")
  console.log(solvedPrint.join('  '))
}
mather()
